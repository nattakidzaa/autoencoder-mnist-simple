import torch
import torch.nn.functional as F
import torch.nn as nn
import torch.optim as optim
import torchvision
from torchvision import datasets, transforms
from torch.utils.data import Dataset
import numpy as np
import os
import sys
import struct
import matplotlib.pyplot as plt
import scipy.misc


def load_mnist(dataset, digits, path, size):
    if dataset == "training":
        fname_img = os.path.join(path, 'train-images-idx3-ubyte')
        fname_lbl = os.path.join(path, 'train-labels-idx1-ubyte')
    elif dataset == "testing":
        fname_img = os.path.join(path, 't10k-images-idx3-ubyte')
        fname_lbl = os.path.join(path, 't10k-labels-idx1-ubyte')
    else:
        raise ValueError("dataset must be 'testing' or 'training'")
    flbl = open(fname_lbl, 'rb')
    magic_nr, size = struct.unpack(">II", flbl.read(8))
    lbl = np.fromfile(flbl, dtype=np.int8)
    flbl.close()
    fimg = open(fname_img, 'rb')
    magic_nr, size, rows, cols = struct.unpack(">IIII", fimg.read(16))
    img = np.fromfile(fimg, dtype=np.uint8).reshape(len(lbl), rows, cols)
    fimg.close()
    return img.reshape([-1, 28, 28, 1]), lbl

# Read data from file to Np array
tr_x_np, tr_y_np = load_mnist(
    dataset='training',
    digits=np.arange(10),
    path='data/',
    size=60000
)
val_x_np, val_y_np = load_mnist(
    dataset='testing',
    digits=np.arange(10),
    path='data/',
    size=10000
)
print("Shape of train data\t\t: {}, {}\nShape of validation data\t: {}, {}".format(
    tr_x_np.shape, tr_y_np.shape, val_x_np.shape, val_y_np.shape
))

# Create Data Loader for Pytorch
tr_dset = torch.utils.data.TensorDataset(
    torch.from_numpy(tr_x_np.transpose([0, 3, 1, 2])).double(),
    torch.from_numpy(tr_y_np.astype('uint8'))
)

tr_loader = torch.utils.data.DataLoader(
    tr_dset,
    batch_size=64,
    shuffle=True,
    num_workers=4,
)

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(784, 400)
        self.fc2 = nn.Linear(400, 200)
        self.fc3 = nn.Linear(200, 100)
        self.fc4 = nn.Linear(100, 50)
        self.fc5 = nn.Linear(50, 20)
        self.fc6 = nn.Linear(20, 10)
        self.fc7 = nn.Linear(10, 20)
        self.fc8 = nn.Linear(20, 50)
        self.fc9 = nn.Linear(50, 100)
        self.fc10 = nn.Linear(100, 200)
        self.fc11 = nn.Linear(200, 400)
        self.fc12 = nn.Linear(400, 784)
    def forward(self, x):
        x = x.view(-1, 784)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = F.relu(self.fc4(x))
        x = F.relu(self.fc5(x))
        x = F.relu(self.fc6(x))
        x = F.relu(self.fc7(x))
        x = F.relu(self.fc8(x))
        x = F.relu(self.fc9(x))
        x = F.relu(self.fc10(x))
        x = F.relu(self.fc11(x))
        x = self.fc12(x)
        return x

torch.set_default_tensor_type('torch.DoubleTensor')
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
net = Net()
net.to(device)
params = net.parameters()
optimizer = optim.Adam(params, lr=1e-3)

epochs = 50
tr_losses = []
val_losses = []
for epoch in range(epochs):
    for i, tr_data in enumerate(tr_loader):
        tr_x, tr_lbl = tr_data
        tr_x, tr_lbl = tr_x.to(device), tr_lbl.to(device)
        optimizer.zero_grad()
        tr_x_hat = net(tr_x.view([-1, 1, 28, 28]))
        diff = tr_x_hat - tr_x.view(-1, 784)
        loss = (torch.sum(torch.mul(diff, diff))/784.0)**0.5
        loss.backward()
        optimizer.step()
        
        tr_loss = loss.item()
        if (i+1) % 100 == 0:
            with torch.no_grad():
                val_x = torch.from_numpy(val_x_np.transpose([0, 3, 1, 2])).double().to(device)
                val_x_hat = net(val_x)
                diff = val_x_hat - val_x.view([-1, 784])
                loss = (torch.sum(torch.mul(diff, diff))/784.0)**0.5
                val_loss = loss.item()
            print("Epoch {} step {:3.0f} : Train Loss - {:12.3f}, Val Loss - {:12.3f}".format(
                epoch+1, i+1, tr_loss, val_loss
            ))
    
    if (epoch + 1) % 5 == 0 or epoch == 0:
        sample_tr_x = tr_x.reshape([-1, 28, 28])[0].cpu().detach().numpy()
        sample_tr_x_hat = tr_x_hat.reshape([-1, 28, 28])[0].cpu().detach().numpy()
        idx = np.random.randint(0, 9999)
        sample_val_x = val_x.reshape([-1, 28, 28])[idx].cpu().detach().numpy()
        sample_val_x_hat = val_x_hat.reshape([-1, 28, 28])[idx].cpu().detach().numpy()
        scipy.misc.imsave('epoch_{}_sample_tr_x.jpg'.format(epoch+1), sample_tr_x)
        scipy.misc.imsave('epoch_{}_sample_tr_x_hat.jpg'.format(epoch+1), sample_tr_x_hat)
        scipy.misc.imsave('epoch_{}_sample_val_x.jpg'.format(epoch+1), sample_val_x)
        scipy.misc.imsave('epoch_{}_sample_val_x_hat.jpg'.format(epoch+1), sample_val_x_hat)
        tr_losses.append(tr_loss)
        val_losses.append(val_loss)

fig = plt.figure(figsize=[16, 6])
ax = fig.add_subplot(1, 2, 1)
ax.plot(np.arange(len(tr_losses)), tr_losses)
ax = fig.add_subplot(1, 2, 2)
ax.plot(np.arange(len(val_losses)), val_losses)
plt.show()
